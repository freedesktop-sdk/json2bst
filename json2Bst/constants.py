BST_BUILDTOOLS = {
    'autotools': {
        'kind': 'kind',
        'description': 'description',
        'depends': 'depends',
        'sources': 'sources',
        'variables': 'variables',
        'config': 'config',
        'environment': 'environment',
        'environment-no-cache': 'environment-no-cache'
    },
    'meson': {
        'kind': 'kind',
        'description': 'description',
        'depends': 'depends',
        'sources': 'sources',
        'variables': 'variables',
        'config': 'config',
        'environment': 'environment',
        'environment-no-cache': 'environment-no-cache'
    },
    'cmake': {
        'kind': 'kind',
        'description': 'description',
        'depends': 'depends',
        'sources': 'sources',
        'variables': 'variables',
        'config': 'config',
        'environment': 'environment',
        'environment-no-cache': 'environment-no-cache'
    }
}

JSON_BUILDTOOL = {
    'autotools': {
        'name': 'name',
        'sources': 'sources',
        'source_type': 'type',
        'variables': ['config-opts'],
        'config': ['build-commands']
    },
    'simple': {
        'name': 'name',
        'sources': 'sources',
        'source_type': 'type',
        'variables': ['config-opts'],
        'config': ['build-commands']
    },
    'meson': {
        'name': 'name',
        'sources': 'sources',
        'source_type': 'type',
        'variables': ['config-opts'],
        'config': ['build-commands']
    },
    'cmake': {
        'name': 'name',
        'sources': 'sources',
        'source_type': 'type',
        'variables': ['config-opts'],
        'config': ['build-commands']
    }
}

SOURCES = {
    'archive': 'tar',
    'bzr': 'bzr',
    'file': 'local',
    'git': 'git',
    'patch': 'patch',
    'script': 'script',
    'shell': 'script'  # treated the same as script
}

SOURCE_PROPERTIES = {
    'type': 'kind',
    'path': 'path',
    'url': 'url',
    'branch': 'track',
    'tag': '',
    'commit': 'ref',
    'sha256': 'ref',
    'dest-filename': '',
    'revision': '',
    'strip-components': '',
    'disable-fsckobjects': '',
    'use-git': '',
    'options': '',
    'commands': ''
}
