#!/usr/bin/env python3
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>
#
#  Based on code originally in Morph, ported to python3, and
#  support for doing this with ruamel instead of pyyaml.
from ruamel import yaml


class BstDumper(yaml.SafeDumper):
    keyorder = (
        # Toplevel symbols
        'kind',
        'description',
        'depends',
        'sources',
        'variables',
        'environment',
        'config',
        'public',

        # Sources
        'url',
        'track',
        'ref',

        # Commands
        'pre-configure-commands',
        'configure-commands',
        'post-configure-commands',
        'pre-build-commands',
        'build-commands',
        'post-build-commands',
        'pre-install-commands',
        'install-commands',
        'post-install-commands',
    )

    def ignore_aliases(self, data):
        return True

    def _iter_in_global_order(cls, mapping):
        for key in cls.keyorder:
            if key in mapping:
                yield key, mapping[key]
        for key in sorted(mapping.keys()):
            if key not in cls.keyorder:
                yield key, mapping[key]

    def _represent_dict(cls, dumper, mapping):
        return dumper.represent_mapping('tag:yaml.org,2002:map',
                                        cls._iter_in_global_order(mapping))

    def _represent_str(cls, dumper, data):
        if data.count('\n') == 0:
            return yaml.representer.SafeRepresenter.represent_str(dumper, data)
        return dumper.represent_scalar(u'tag:yaml.org,2002:str',
                                       data, style='|')

    def _represent_bytes(cls, dumper, orig_data):
        fallback_representer = yaml.representer.SafeRepresenter.represent_bytes
        try:
            data = orig_data.decode('ascii')
            if data.count('\n') == 0:
                return fallback_representer(dumper, orig_data)
        except UnicodeDecodeError:
            try:
                data = orig_data.decode('utf-8')
                if data.count('\n') == 0:
                    return fallback_representer(dumper, orig_data)
            except UnicodeDecodeError:
                return fallback_representer(dumper, orig_data)
        return dumper.represent_scalar(u'tag:yaml.org,2002:str',
                                       data, style='|')

    def __init__(self, *args, **kwargs):
        yaml.SafeDumper.__init__(self, *args, **kwargs)
        self.add_representer(dict, self._represent_dict)
        self.add_representer(str, self._represent_str)
        self.add_representer(bytes, self._represent_bytes)
        roundtrip = yaml.representer.RoundTripRepresenter
        representer = roundtrip.represent_preserved_scalarstring
        self.add_representer(
            yaml.scalarstring.PreservedScalarString,
            representer)


def bst_dump(node, filename):
    with open(filename, 'w') as f:
        yaml.dump(node, f, Dumper=BstDumper,
                  default_flow_style=False, line_break="\n")
