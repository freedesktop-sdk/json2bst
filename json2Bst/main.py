#!/usr/bin/python3

import argparse
import json
import sys
import os.path
import constants as cst

from jsoncomment import JsonComment
from modules.module import Module
from sources.source import Source
from tools.bstWriter import BstWriter


def stop_execution(error=""):
    """Stop execution of script with optional message"""
    if error != "":
        sys.exit(error)
    else:
        sys.exit()


def load_json_file(file_path):
    """load in raw json data from a file"""
    # overload json parser to remove comments
    parser = JsonComment(json)

    with open(file_path) as raw_json:
        json_data = parser.load(raw_json)

    return json_data


def retrieve_module_list(raw_json, module_property):
    """Takes a json file and returns a list located at 'module property'"""
    return raw_json[module_property]


def parse_module(json_module, buildtool):
    """Takes a json module and converts it to a bst module"""

    try:
        bst_module = Module(json_module[cst.JSON_BUILDTOOL[buildtool]['name']],
                            buildtool)
    except KeyError:
        print('Module {0} has no name'.format(module))
        raise KeyError

    bst_sources = parse_sources(
        json_module[cst.JSON_BUILDTOOL[buildtool]['sources']],
        bst_module.buildtool())

    if bst_sources != []:
        bst_module.sources = bst_sources
    else:
        print('No sources found for {0}'.format(bst_module.name))

    bst_config = parse_config_commands(json_module, bst_module.buildtool())

    bst_variables = parse_variables(json_module, bst_module.buildtool())

    if bst_config != {}:
        bst_module.config = bst_config

    if bst_variables != {}:
        bst_module.variables = bst_variables

    return bst_module


def parse_sources(json_sources, buildtool):
    """Returns a list of source objects from json"""
    bst_sources = []

    for source in json_sources:
        source_obj = parse_source(source, buildtool)

        if source_obj:
            bst_sources.append(source_obj)
        else:
            print('Error failed to parse {0}'.format(source))

    return bst_sources


def parse_source(source, buildtool):
    """Parses raw source data into a bst compliant Source object"""

    try:
        source_type = source[cst.JSON_BUILDTOOL[buildtool]['source_type']]
    except KeyError:
        print('No source type found in source')
        return None

    try:
        source_obj = Source(cst.SOURCES[source_type])
        # type is now converted so pop it from the object
        source.pop(cst.JSON_BUILDTOOL[buildtool]['source_type'])
    except KeyError:
        print('No valid source conversion found for {0} in \
                constants.py'.format(source_type))
        return None

    for attribute in source:

        bst_key = cst.SOURCE_PROPERTIES[attribute]

        if not bst_key:
            bst_key = attribute
            # Add key name to incompatable list in source object
            source_obj.incompatable.append(attribute)

        try:
            attribute_value = source[attribute]
        except KeyError:
            attribute_value = ''

        if bst_key and attribute_value:
            source_obj.attributes[bst_key] = attribute_value

    return source_obj


def parse_config_commands(json_module, buildtool):
    """Returns a {} of config options defined for the build_system"""

    config = {}

    for conf_option in cst.JSON_BUILDTOOL[buildtool]['config']:
        if conf_option in json_module:
            config[conf_option] = json_module[conf_option]

    return config


def parse_variables(json_module, buildtool):
    """convert any specified options into variables"""
    variables = {}

    for variable in cst.JSON_BUILDTOOL[buildtool]['variables']:
        if variable in json_module:
            variables[variable] = json_module[variable]

    return variables


if __name__ == '__main__':

    # Command line config
    parser = argparse.ArgumentParser()
    parser.add_argument('--file-name', help='Name of json file to be parsed')
    parser.add_argument('--json-module-tag',
                        help='ID of Json property for module location')
    parser.add_argument('--definitions-dir',
                        help='Directory to store bst definitions')

    args = parser.parse_args()

    if args.file_name is None and args.json_module_tag is None:
        stop_execution(
            "Must define a Json file & Module identifier, --help for info'")

    bst_modules = []

    # Parse json
    json_file = load_json_file(args.file_name)
    modules = retrieve_module_list(json_file, args.json_module_tag)

    # Add dependencies to module
    for idx, module in enumerate(modules):

        if 'buildsystem' in module:
            if module['buildsystem'] != 'simple':
                build_system = module['buildsystem']
        elif 'autotools' in module:
            if module['autotools']:
                build_system = 'autotools'
        elif 'meson' in module:
            if module['meson']:
                build_system = 'meson'
        elif 'cmake' in module:
            if module['cmake']:
                build_system = 'cmake'
        else:
            build_system = 'autotools'

        bst_module = parse_module(module, build_system)
        bst_module.depends = bst_modules[:idx]
        bst_modules.append(bst_module)

    definitions_dir = os.path.expanduser(
        args.definitions_dir) if args.definitions_dir else os.path.curdir

    writer = BstWriter()

    for module in bst_modules:
        writer.write_to_bst(definitions_dir, module)
